/* **********************************************************************\
 *                                                                      *
 *  #   Name:   TreetoHists      	                                *
 *                                                                      *
 *  #   Date    Comments                   By                           *
 * -- -------- -------------------------- ----------------------------- *
 *  1 16/08/17 First version             R. Devesa (mdevesa@cern.ch)    *
 *  2 22/09/17 Second version            R. Devesa (mdevesa@cern.ch)    *
 *  3 31/01/18 Third version             R. Devesa (mdevesa@cern.ch)    *
 *  4 08/04/18 Fourth version            F. Daneri (mdaneri@cern.ch)    *
\************************************************************************/

#include "Trig.h"

int main(int argc, char* argv[]) {
 
  std::string inputFileUser = "";

  //---------------------------
  // Decoding the user settings
  //---------------------------
  for (int i=1; i< argc; i++){

    std::string opt(argv[i]); std::vector< std::string > v;

    std::istringstream iss(opt);

    std::string item;
    char delim = '=';

    while (std::getline(iss, item, delim)){
        v.push_back(item);
    }

    if ( opt.find("--debug=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_debug= true;
      if (v[1].find("FALSE") != std::string::npos) m_debug= false;
    }
    if ( opt.find("--is17=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_data17 = true;
      if (v[1].find("FALSE") != std::string::npos) m_data17 = false;
    }
    if ( opt.find("--is15=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_data15 = true;
      if (v[1].find("FALSE") != std::string::npos) m_data15 = false;
    }
    if ( opt.find("--isMC=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_MC= true;
      if (v[1].find("FALSE") != std::string::npos) m_MC= false;
    }
    if ( opt.find("--isSignal=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_signal= true;
      if (v[1].find("FALSE") != std::string::npos) m_signal= false;
    }
    if ( opt.find("--onGrid=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_onGrid= true;
      if (v[1].find("FALSE") != std::string::npos) m_onGrid= false;
    }
    if ( opt.find("--isjjj=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_isjjj= true;
      if (v[1].find("FALSE") != std::string::npos) m_isjjj= false;
    }
  }//End: Loop over input options
 
  TStopwatch timer; 
  timer.Start(); 
//   gROOT->Reset(); 
  //gROOT->ProcessLine(".L loader.C+"); 
  
  Initialize();
  
  //-----------------------
  // Getting File
  //-----------------------
   
  std::vector<TString> Files;
  GetListOfRootFiles(Files);
  
  std::cout << "Files.size()"<<Files.size() << std::endl;
  //-----------------------
  // Getting Tree
  //-----------------------
  TChain tree("outTree");
  
  for (unsigned int i_file = 0; i_file != Files.size(); ++i_file) {
    tree.Add(Files.at(i_file).Data());
  }
    
  //---------------------------
  // Enable necessary branches
  //---------------------------
  
  Enable_and_Set_Branches(tree);
    
  // Number of events
  Long64_t entries = tree.GetEntries();
    
  if(m_debug) std::cout << "entries:"<< entries << std::endl;
  
  //------------------
  // Loop over entries
  //------------------
  if(m_debug) std::cout << "Loop over entries" << std::endl;
  for (Long64_t entry=0; entry < entries; ++entry) {
    
    bool should_skip = false; // Jet Cleaning
    
    tree.GetEntry(entry);
    h_cutflow->Fill(0); // Total Number of events
    h_cutflow->GetXaxis()->SetBinLabel(1,"AllEvent") ;
    //Show status
    if(entry % 1000000 == 0) std::cout << "Entry = " << entry << " of " << entries << std::endl;

    //---------------------
    // event weight
    //---------------------
    double weight = 1.;
    //MC
    if(m_MC) weight = m_weight/entries;
    //Data
    if(!m_MC) weight = 1;
    
    //-------------
    //   Triggers
    //-------------
//     for (int j=0 ; j < m_passedTriggers->size();++j){
//       //if (m_passedTriggers->at(j)=="HLT_3j200"){      
// std::cout << "m_passedTriggers: " << m_passedTriggers->at(j) << std::endl; 
//       //}
//     }

    /// Loop over reco jets
    Jets.clear();
    for (unsigned int ii = 0; ii != m_jet_pt->size(); ++ii) {
	jet thisJet;
	double pt  = m_jet_pt ->at(ii);
	double eta = m_jet_eta->at(ii);
	double phi = m_jet_phi->at(ii);
	double E   = m_jet_E  ->at(ii);
	
	thisJet.jet.SetPtEtaPhiE(pt,eta,phi,E);
	thisJet.clean_passLooseBad = m_jet_clean_passLooseBad->at(ii);
	    
	Jets.push_back(thisJet);
    } //END: Loop over jets
    
    if(Jets.size()==0){continue;}
    h_cutflow->Fill(1); // Total Number of events
    h_cutflow->GetXaxis()->SetBinLabel(2,"notcerojets") ;
     
    if(m_passedTriggers->size()==0){continue;}

    if(m_isjjj){
      if(Jets.size()<3){continue;}
      h_cutflow->GetXaxis()-> SetBinLabel(3,"morethan2jets") ;
      h_cutflow->Fill(2); // Total Number of events
    }else{
      //if(Jets.size()<2){continue;}
      if(Jets.size()<3){continue;}//only for compound triggers
      if (m_ph_pt->size()==0){continue;}
      h_cutflow->GetXaxis()-> SetBinLabel(3,"morethan1jetsand1gamma") ;
      h_cutflow->Fill(2); // Total Number of events
    }
    //control plots
    fillControlHistos(Jets,weight);


    ////////////////////////
    // Fill histograms    //
    ////////////////////////

	
    for (unsigned int kk=0 ;kk<trigger_name.size() ; ++kk){
	
      double pt;
      double pt1;
      double pt2;
      double pt3;

      if (m_isjjj){
	if (trigger_type.at(kk)=="single_jet_trigger" || trigger_type.at(kk)=="single_ht_jet_trigger")    {pt = Jets.at(0).jet.Pt();}
	if (trigger_type.at(kk)=="multi_jet_trigger")     {pt = Jets.at(2).jet.Pt();}
	if (trigger_type.at(kk)=="multi_jet_trigger_btag"){pt = Jets.at(1).jet.Pt();}
      }else{
	if (trigger_type.at(kk)=="single_jet_trigger"){	
		pt = m_ph_pt->at(0);
		pt1 = Jets.at(0).jet.Pt();
		pt2 = Jets.at(1).jet.Pt();
		pt3 = Jets.at(2).jet.Pt();}
      }
      
      //Fill only T
      TString nam = TString::Format("h_pass_%s_n_%i_for_all__%s" ,trigger_name.at(kk).Data(), kk,trigger_type.at(kk).Data());
      //if (passTrig(trigger_name.at(kk))  ){ fillHistos(nam ,pt, weight);}
      
      if(passTrig(trigger_prescaled_name.at(kk))){
	
	//Fill denominator    
	if(m_isjjj){fillHistos("h_pass_" + trigger_prescaled_name.at(kk)+"_for_"+trigger_name.at(kk)+"__"+trigger_type.at(kk) ,pt, weight);}
        if(!m_isjjj){
          // To apply cuts on leading/subleading pT photon or jet uncomment next line
	  //if(pt2>200){
	  //if(pt>100){
		fillHistos("h_pass_" + trigger_prescaled_name.at(kk)+"_for_"+trigger_name.at(kk)+"__"+trigger_type.at(kk)+"_ph" ,pt, weight);
		fillHistos("h_pass_" + trigger_prescaled_name.at(kk)+"_for_"+trigger_name.at(kk)+"__"+trigger_type.at(kk)+"_jet1" ,pt1, weight);
		fillHistos("h_pass_" + trigger_prescaled_name.at(kk)+"_for_"+trigger_name.at(kk)+"__"+trigger_type.at(kk)+"_jet2",pt2, weight);
		fillHistos("h_pass_" + trigger_prescaled_name.at(kk)+"_for_"+trigger_name.at(kk)+"__"+trigger_type.at(kk)+"_jet3" ,pt3, weight);
	  //}
	}
	if(passTrig(trigger_name.at(kk))){  	  
	  if(m_isjjj){fillHistos("h_pass_"+trigger_name.at(kk)+"_and_"+trigger_prescaled_name.at(kk)+"__"+trigger_type.at(kk), pt, weight);}
	  if(!m_isjjj){
            // To apply cuts on leading/subleading pT photon or jet uncomment next line
            //if(pt2>200){
            //if(pt>100){
		fillHistos("h_pass_"+trigger_name.at(kk)+"_and_"+trigger_prescaled_name.at(kk)+"__"+trigger_type.at(kk)+"_ph", pt, weight);
	  	fillHistos("h_pass_"+trigger_name.at(kk)+"_and_"+trigger_prescaled_name.at(kk)+"__"+trigger_type.at(kk)+"_jet1", pt1, weight);
	 	fillHistos("h_pass_"+trigger_name.at(kk)+"_and_"+trigger_prescaled_name.at(kk)+"__"+trigger_type.at(kk)+"_jet2", pt2, weight);
	 	fillHistos("h_pass_"+trigger_name.at(kk)+"_and_"+trigger_prescaled_name.at(kk)+"__"+trigger_type.at(kk)+"_jet3", pt3, weight);
	    //}
	  }
	};// end passTrig("")
      }
    }	
	

    //------------------------------ 
    // Cleaning values
    //------------------------------
    
    if(m_debug) std::cout << "Cleaning vectors" << std::endl;

    m_runNumber = 999;
    m_eventNumber = 999;
    m_lumiBlock = 999;
    m_NPV = 999;
    m_weight_pileup = 999;
    m_weight = 999.;
    m_weight_xs = 999.;


    // Cleaning vectors
    m_jet_clean_passLooseBad->clear();
    m_jet_pt->clear();
    m_jet_eta->clear();
    m_jet_phi->clear();
    m_jet_E->clear();
    m_passedTriggers->clear();

    if (!m_isjjj){
      m_ph_pt ->clear();
      m_ph_eta ->clear();
      m_ph_phi ->clear();
    }
    
  
    if(m_debug) std::cout << "Entry analyzed" << std::endl;
    if(m_debug) std::cout << "Entry number:"<< entry << std::endl;

  } // End: Loop over entries

  //------------------------------------
  // Saving Histogramas into a ROOT file
  //------------------------------------
  
  std::cout << "Saving plots into a ROOT file..." << std::endl;

  // Output file name
  TString outputfile("");
  
  if (m_isjjj){ outputfile += "jjj_";} else{outputfile += "gjj_";}
  
  if(m_MC && m_signal)         { outputfile += "Signal.root";}
  else if(m_MC && !m_signal)   { outputfile += "MC.root"    ;}
  else if (!m_MC && m_data17 && !m_data15) { outputfile += "Data17.root";}
  else if (!m_MC && !m_data17 && !m_data15){ outputfile += "Data16.root";}
  else if (!m_MC && !m_data17 && m_data15){ outputfile += "Data15.root";}
  
  // Opening output file
  TFile* tout = new TFile(outputfile,"recreate");
  std::cout << "output file = " << outputfile << std::endl;
  tout->cd();

  //Writing histograms
  h_cutflow  ->Write();
  //control plots
  h_jet0_pt  ->Write();
  h_jet0_eta ->Write();
  h_jet0_phi ->Write();
  h_jet1_pt  ->Write();
  h_jet1_eta ->Write();
  h_jet1_phi ->Write();
  h_jet2_pt  ->Write();
  h_jet2_eta ->Write();
  h_jet2_phi ->Write();

  
  /// SAVE pass histograms
  std::map<TString, TH1D *>::iterator it;                                                                                                                      
  for (it = m_pass.begin(); it != m_pass.end(); ++it) {
      (it->second)->Write();
  }

  tout->Close();


  Terminate();
  return 0;

}




/*
 *
 *
 * Personal Functions
 *
 *
 *
 * */

bool fillControlHistos (const std::vector<jet> & Jets   ,const float & weight){
   /// Fill Histos

  if (m_isjjj){
    h_jet0_pt ->Fill(Jets.at(0).jet.Pt() , weight);    h_jet1_pt ->Fill(Jets.at(1).jet.Pt() , weight);      h_jet2_pt ->Fill(Jets.at(2).jet.Pt() , weight);
    h_jet0_eta->Fill(Jets.at(0).jet.Eta(), weight);    h_jet1_eta->Fill(Jets.at(1).jet.Eta(), weight);      h_jet2_eta->Fill(Jets.at(2).jet.Eta(), weight);
    h_jet0_phi->Fill(Jets.at(0).jet.Phi(), weight);    h_jet1_phi->Fill(Jets.at(1).jet.Phi(), weight);      h_jet2_phi->Fill(Jets.at(2).jet.Phi(), weight);
  }else{
    h_jet0_pt ->Fill(Jets.at(0).jet.Pt() , weight);    h_jet1_pt ->Fill(Jets.at(1).jet.Pt() , weight);     
    h_jet0_eta->Fill(Jets.at(0).jet.Eta(), weight);    h_jet1_eta->Fill(Jets.at(1).jet.Eta(), weight);     
    h_jet0_phi->Fill(Jets.at(0).jet.Phi(), weight);    h_jet1_phi->Fill(Jets.at(1).jet.Phi(), weight);     
  }  
  return true;
}



bool passTrig(TString trigName){
  
  
  return (find(m_passedTriggers->begin(), m_passedTriggers->end(), trigName ) != m_passedTriggers->end());
  

}

void fillHistos(const TString & name, const float & jetPt, const float & weight)
{
  
    /// Fill Histos
    if (m_pass[name] == 0) {std::cout << "ALERTA: histograma vacio " << name << std::endl;}
    m_pass[name]->Fill(jetPt, weight);
    
}




void Enable_and_Set_Branches(TChain & tree){

  tree.SetBranchStatus("*",0); //disable all branches


  tree.SetBranchStatus  ("runNumber",    1);
  tree.SetBranchAddress ("runNumber",    &m_runNumber);

  tree.SetBranchStatus  ("eventNumber",    1);
  tree.SetBranchAddress ("eventNumber",    &m_eventNumber);

  tree.SetBranchStatus  ("lumiBlock",    1);
  tree.SetBranchAddress ("lumiBlock",    &m_lumiBlock);

  tree.SetBranchStatus  ("NPV",    1);
  tree.SetBranchAddress ("NPV",    &m_NPV);

  tree.SetBranchStatus  ("weight_pileup", 1);
  tree.SetBranchAddress ("weight_pileup", &m_weight_pileup);

  tree.SetBranchStatus  ("jet_clean_passLooseBad", 1);
  tree.SetBranchAddress ("jet_clean_passLooseBad", &m_jet_clean_passLooseBad);

  tree.SetBranchStatus  ("passedTriggers", 1);
  tree.SetBranchAddress ("passedTriggers", &m_passedTriggers);

  tree.SetBranchStatus  ("weight", 1);
  tree.SetBranchAddress ("weight", &m_weight);

  tree.SetBranchStatus  ("weight_xs", 1);
  tree.SetBranchAddress ("weight_xs", &m_weight_xs);


  tree.SetBranchStatus  ("jet_pt", 1);
  tree.SetBranchAddress ("jet_pt", &m_jet_pt);

  tree.SetBranchStatus  ("jet_eta", 1);
  tree.SetBranchAddress ("jet_eta", &m_jet_eta);

  tree.SetBranchStatus  ("jet_phi", 1);
  tree.SetBranchAddress ("jet_phi", &m_jet_phi);

  tree.SetBranchStatus  ("jet_E", 1);
  tree.SetBranchAddress ("jet_E", &m_jet_E);

  if (!m_isjjj){
    
    tree.SetBranchStatus  ("ph_pt", 1); 
    tree.SetBranchAddress ("ph_pt", &m_ph_pt); 
   
    tree.SetBranchStatus  ("ph_phi", 1); 
    tree.SetBranchAddress ("ph_phi", &m_ph_phi); 

    tree.SetBranchStatus  ("ph_eta", 1); 
    tree.SetBranchAddress ("ph_eta", &m_ph_eta); 

  }
}//END: EnableBranches()



//****************************************************************************+
void Initialize()
{

    int nBins = 800;    int nBins_eta = 600 ;  int nBins_phi = 600;
    float min = 0;      float min_eta = -3  ;  float min_phi = 0;
    float max = 800;    float max_eta = 3   ;  float max_phi = 6;

    trigger_name.clear(); trigger_prescaled_name.clear();   trigger_type.clear();
    //Basic Triggers

     if (m_isjjj){
      //trigger_name.push_back("HLT_j380");                               trigger_prescaled_name.push_back("HLT_j110");           trigger_type.push_back("single_jet_trigger");
      //trigger_name.push_back("HLT_j380");                               trigger_prescaled_name.push_back("HLT_j260");           trigger_type.push_back("single_jet_trigger");  
      //trigger_name.push_back("HLT_j400");                               trigger_prescaled_name.push_back("HLT_j260");           trigger_type.push_back("single_jet_trigger");
      //trigger_name.push_back("HLT_j400");                               trigger_prescaled_name.push_back("HLT_j110");           trigger_type.push_back("single_jet_trigger");
      //trigger_name.push_back("HLT_j400");                               trigger_prescaled_name.push_back("HLT_j100");           trigger_type.push_back("single_jet_trigger");

      //trigger_name.push_back("HLT_j420");                               trigger_prescaled_name.push_back("HLT_j260");           trigger_type.push_back("single_jet_trigger");
      //trigger_name.push_back("HLT_j420");                               trigger_prescaled_name.push_back("HLT_j110");           trigger_type.push_back("single_jet_trigger");
      //trigger_name.push_back("HLT_j420");                               trigger_prescaled_name.push_back("HLT_j100");           trigger_type.push_back("single_jet_trigger");

      //trigger_name.push_back("HLT_j440");                               trigger_prescaled_name.push_back("HLT_j110");           trigger_type.push_back("single_jet_trigger");
      //trigger_name.push_back("HLT_j440");                               trigger_prescaled_name.push_back("HLT_j100");           trigger_type.push_back("single_jet_trigger");  

      //trigger_name.push_back("HLT_3j200");                              trigger_prescaled_name.push_back("HLT_j110");           trigger_type.push_back("multi_jet_trigger");
      //trigger_name.push_back("HLT_3j200");                              trigger_prescaled_name.push_back("HLT_j100");           trigger_type.push_back("multi_jet_trigger");
      //trigger_name.push_back("HLT_3j200");                              trigger_prescaled_name.push_back("HLT_3j175");          trigger_type.push_back("multi_jet_trigger");

      //trigger_name.push_back("HLT_3j175");                              trigger_prescaled_name.push_back("HLT_j110");           trigger_type.push_back("multi_jet_trigger");

      trigger_name.push_back("HLT_j225_gsc400_boffperf_split");         trigger_prescaled_name.push_back("HLT_j110");           trigger_type.push_back("single_jet_trigger");
      //trigger_name.push_back("HLT_j225_gsc420_boffperf_split");         trigger_prescaled_name.push_back("HLT_j110");           trigger_type.push_back("single_jet_trigger");
    }else{
      trigger_name.push_back("HLT_g140_loose");                         trigger_prescaled_name.push_back("HLT_g60_loose");     trigger_type.push_back("single_jet_trigger");
      //trigger_name.push_back("HLT_g120_loose");                         trigger_prescaled_name.push_back("HLT_g50_loose");      trigger_type.push_back("single_jet_trigger");
      //trigger_name.push_back("HLT_g75_tight_3j50noL1_L1EM22VHI");       trigger_prescaled_name.push_back("HLT_g60_loose");      trigger_type.push_back("single_jet_trigger");//data16
      //trigger_name.push_back("HLT_g20_loose_L1EM18VH_2j40_0eta490_3j25_0eta490_invm700");       trigger_prescaled_name.push_back("HLT_g10_loose");      trigger_type.push_back("single_jet_trigger");//data15
      //trigger_name.push_back("HLT_g85_tight_L1EM22VHI_3j50noL1");       trigger_prescaled_name.push_back("HLT_g60_loose");      trigger_type.push_back("single_jet_trigger");//data17
    }
  
      
    for (unsigned int l = 0; l < trigger_name.size(); ++l){
 
	//num name  (T and T0)
	TString num_name = "h_pass_"+ trigger_name.at(l) +"_and_"+trigger_prescaled_name.at(l);
	//den name (T0)
	TString den_name = "h_pass_"+ trigger_prescaled_name.at(l)+"_for_"+trigger_name.at(l);
	//num only (T)
        TString nam = TString::Format("h_pass_%s_n_%i_for_all" ,trigger_name.at(l).Data(), l);
	
	
	if (m_isjjj){
	if (trigger_type.at(l)=="single_jet_trigger"){
          
	  m_pass[num_name+"__"+trigger_type.at(l)]  = new TH1D(num_name+"__"+trigger_type.at(l), num_name+"__"+trigger_type.at(l),  800, 200, 600); 
	  m_pass[num_name+"__"+trigger_type.at(l)] ->Sumw2();  
	  
	  m_pass[den_name+"__"+trigger_type.at(l)]  = new TH1D(den_name+"__"+trigger_type.at(l), den_name+"__"+trigger_type.at(l), 800, 200, 600);  
	  m_pass[den_name+"__"+trigger_type.at(l)]->Sumw2();  
	  
	  m_pass[nam+"__"+trigger_type.at(l)] = new TH1D(nam+"__"+trigger_type.at(l),nam+"__"+trigger_type.at(l),  800, 200, 600); 
	  m_pass[nam+"__"+trigger_type.at(l)] ->Sumw2(); 
	  
	}else if (trigger_type.at(l)=="multi_jet_trigger"){
	  m_pass[num_name+"__"+trigger_type.at(l)]  = new TH1D(num_name+"__"+trigger_type.at(l),num_name+"__"+trigger_type.at(l),  800, 0, 400); 
	  m_pass[num_name+"__"+trigger_type.at(l)] ->Sumw2();  
	  
	  m_pass[den_name+"__"+trigger_type.at(l)]  = new TH1D(den_name+"__"+trigger_type.at(l), den_name+"__"+trigger_type.at(l), 800, 0, 400);   
	  m_pass[den_name+"__"+trigger_type.at(l)]->Sumw2();  
	  
	  m_pass[nam+"__"+trigger_type.at(l)] = new TH1D(nam+"__"+trigger_type.at(l),nam+"__"+trigger_type.at(l),  800, 0, 400); 
	  m_pass[nam+"__"+trigger_type.at(l)] ->Sumw2(); 
	  
	}else if (trigger_type.at(l)=="single_ht_jet_trigger"){
	  m_pass[num_name+"__"+trigger_type.at(l)]  = new TH1D(num_name+"__"+trigger_type.at(l), num_name+"__"+trigger_type.at(l),  800, 350, 750); 
	  m_pass[num_name+"__"+trigger_type.at(l)] ->Sumw2();  
	  
	  m_pass[den_name+"__"+trigger_type.at(l)]  = new TH1D(den_name+"__"+trigger_type.at(l), den_name+"__"+trigger_type.at(l),  800, 350, 750);  
	  m_pass[den_name+"__"+trigger_type.at(l)]->Sumw2();  
	  
	  m_pass[nam+"__"+trigger_type.at(l)] = new TH1D(nam+"__"+trigger_type.at(l),nam+"__"+trigger_type.at(l),  800, 350, 750); 
	  m_pass[nam+"__"+trigger_type.at(l)] ->Sumw2(); 
	  
	}
	}else{
	  m_pass[num_name+"__"+trigger_type.at(l)+"_ph"]  = new TH1D(num_name+"__"+trigger_type.at(l)+"_ph", num_name+"__"+trigger_type.at(l)+"_ph",  150, 0, 300); 
	  m_pass[num_name+"__"+trigger_type.at(l)+"_ph"]->Sumw2();  
	  
	  m_pass[den_name+"__"+trigger_type.at(l)+"_ph"]  = new TH1D(den_name+"__"+trigger_type.at(l)+"_ph", den_name+"__"+trigger_type.at(l)+"_ph", 150, 0, 300);  
	  m_pass[den_name+"__"+trigger_type.at(l)+"_ph"]->Sumw2();  
	  
          m_pass[num_name+"__"+trigger_type.at(l)+"_jet1"]  = new TH1D(num_name+"__"+trigger_type.at(l)+"_jet1", num_name+"__"+trigger_type.at(l)+"_jet1",  200, 0, 400); 
	  m_pass[num_name+"__"+trigger_type.at(l)+"_jet1"] ->Sumw2();  
	  
	  m_pass[den_name+"__"+trigger_type.at(l)+"_jet1"]  = new TH1D(den_name+"__"+trigger_type.at(l)+"_jet1", den_name+"__"+trigger_type.at(l)+"_jet1", 200, 0, 400);  
	  m_pass[den_name+"__"+trigger_type.at(l)+"_jet1"]->Sumw2();  
	  
	  m_pass[num_name+"__"+trigger_type.at(l)+"_jet2"]  = new TH1D(num_name+"__"+trigger_type.at(l)+"_jet2", num_name+"__"+trigger_type.at(l)+"_jet2",  200, 0, 400); 
	  m_pass[num_name+"__"+trigger_type.at(l)+"_jet2"]->Sumw2();  
	  
	  m_pass[den_name+"__"+trigger_type.at(l)+"_jet2"]  = new TH1D(den_name+"__"+trigger_type.at(l)+"_jet2", den_name+"__"+trigger_type.at(l)+"_jet2", 200, 0, 400);  
	  m_pass[den_name+"__"+trigger_type.at(l)+"_jet2"]->Sumw2();  
	  
          m_pass[num_name+"__"+trigger_type.at(l)+"_jet3"]  = new TH1D(num_name+"__"+trigger_type.at(l)+"_jet3", num_name+"__"+trigger_type.at(l)+"_jet3",  200, 0, 400); 
	  m_pass[num_name+"__"+trigger_type.at(l)+"_jet3"]->Sumw2();  
	  
	  m_pass[den_name+"__"+trigger_type.at(l)+"_jet3"]  = new TH1D(den_name+"__"+trigger_type.at(l)+"_jet3", den_name+"__"+trigger_type.at(l)+"_jet3", 200, 0, 400);  
	  m_pass[den_name+"__"+trigger_type.at(l)+"_jet3"]->Sumw2();  

	  //m_pass[nam+"__"+trigger_type.at(l)] = new TH1D(nam+"__"+trigger_type.at(l),nam+"__"+trigger_type.at(l),  600, 0, 300); 
	  //m_pass[nam+"__"+trigger_type.at(l)] ->Sumw2(); 	  
	}
    }

  
    h_cutflow = new TH1D("Cutflow","",11,-0.5,10.5);    h_cutflow->Sumw2();
    
    h_jet0_pt   =  new TH1D("jet0_pt",  "jet0_pt" ,  nBins, min, max);                   h_jet0_pt    ->Sumw2();
    h_jet0_eta  =  new TH1D("jet0_eta", "jet0_eta",  nBins_eta, min_eta, max_eta);       h_jet0_eta   ->Sumw2();
    h_jet0_phi  =  new TH1D("jet0_phi", "jet0_phi",  nBins_phi, min_phi, max_phi);       h_jet0_phi   ->Sumw2();
    h_jet1_pt   =  new TH1D("jet1_pt",  "jet1_pt" ,  nBins, min, max);                   h_jet1_pt    ->Sumw2();
    h_jet1_eta  =  new TH1D("jet1_eta", "jet1_eta",  nBins_eta, min_eta, max_eta);       h_jet1_eta   ->Sumw2();
    h_jet1_phi  =  new TH1D("jet1_phi", "jet1_phi",  nBins_phi, min_phi, max_phi);       h_jet1_phi   ->Sumw2();
    h_jet2_pt   =  new TH1D("jet2_pt" , "jet2_pt" ,  nBins, min, max);                   h_jet2_pt    ->Sumw2();
    h_jet2_eta  =  new TH1D("jet2_eta", "jet2_eta",  nBins_eta, min_eta, max_eta);       h_jet2_eta   ->Sumw2();
    h_jet2_phi  =  new TH1D("jet2_phi", "jet2_phi",  nBins_phi, min_phi, max_phi);       h_jet2_phi   ->Sumw2(); 
 
}//END: Initialize()


void Terminate(void)
{
    std::map<TString, TH1D *>::iterator it;
    for (it = m_pass.begin(); it != m_pass.end(); ++it) {
        delete (it->second);
    }
}


void GetListOfRootFiles ( std::vector< TString> & Files)
{

  TString pathFiles="";

  if(!m_MC && !m_data17 && m_isjjj && !m_signal && m_data15) {

    if (m_onGrid==false){ pathFiles="/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijetISR/ntuples/data_trijet/";} // trijet-data15

    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodA.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055127._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodA.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055127._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodA.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055127._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055131._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055131._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055131._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055131._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055131._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055131._000006.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055131._000007.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055131._000008.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055131._000009.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055134._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055134._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055134._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055134._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055134._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055134._000006.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055134._000007.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055134._000008.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055134._000009.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055134._000010.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055134._000011.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055134._000012.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055134._000013.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055134._000014.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055134._000015.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055134._000016.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055134._000017.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055134._000018.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055138._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055138._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055138._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055138._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055138._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055138._000006.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055138._000007.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055138._000008.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055138._000009.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055138._000010.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055138._000011.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055138._000012.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055138._000013.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055138._000014.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055138._000015.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055138._000016.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055138._000017.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055138._000018.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055142._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055142._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055142._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055142._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055142._000006.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055142._000007.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055142._000008.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055145._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055145._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055145._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055145._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055145._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055145._000006.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055145._000007.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055145._000008.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055145._000009.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055145._000010.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055145._000011.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055145._000012.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055145._000013.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055145._000014.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055145._000015.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055145._000016.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055145._000017.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055145._000018.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055145._000019.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodH.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055148._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodH.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055148._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodH.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055148._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodH.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055148._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodH.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055148._000006.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodH.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055148._000007.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055153._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055153._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055153._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055153._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055153._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055153._000006.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055153._000007.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055153._000008.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055153._000009.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055153._000010.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055153._000011.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055153._000012.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055153._000013.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055153._000014.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055153._000015.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055153._000016.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055153._000017.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055153._000018.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055153._000019.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055153._000020.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055153._000021.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055153._000022.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055153._000023.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055153._000024.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055153._000025.tree.root");

  } else if(!m_MC && !m_data17 && !m_isjjj && !m_signal && m_data15) {
   
    if (m_onGrid==false){ pathFiles="/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijetISR/ntuples/data_dijetgamma/";} // dijet gamma-data15

    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodA.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054910._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodA.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054910._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodA.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054910._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodA.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054910._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodA.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054910._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodA.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054910._000006.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodA.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054910._000007.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodC.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054914._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodC.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054914._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodC.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054914._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodC.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054914._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodC.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054914._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodC.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054914._000006.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodC.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054914._000007.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodC.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054914._000008.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodC.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054914._000009.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodC.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054914._000010.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodC.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054914._000011.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodC.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054914._000012.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodC.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054914._000013.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodC.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054914._000014.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodD.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054917._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodD.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054917._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodD.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054917._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodD.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054917._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodD.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054917._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodD.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054917._000006.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodD.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054917._000007.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodD.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054917._000008.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodD.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054917._000009.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodD.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054917._000010.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodD.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054917._000011.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodD.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054917._000012.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodD.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054917._000013.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodD.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054917._000014.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodD.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054917._000015.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodD.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054917._000016.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodD.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054917._000017.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodD.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054917._000018.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodD.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054917._000019.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodD.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054917._000020.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodE.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054922._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodE.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054922._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodE.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054922._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodE.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054922._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodE.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054922._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodE.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054922._000006.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodE.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054922._000007.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodE.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054922._000008.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodE.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054922._000009.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodE.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054922._000010.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodE.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054922._000011.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodE.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054922._000012.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodE.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054922._000013.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodE.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054922._000014.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodE.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054922._000015.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodE.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054922._000016.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodE.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054922._000017.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodE.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054922._000018.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodF.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054927._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodF.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054927._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodF.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054927._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodF.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054927._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodF.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054927._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodF.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054927._000006.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodG.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054929._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodG.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054929._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodG.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054929._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodG.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054929._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodG.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054929._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodG.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054929._000006.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodG.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054929._000007.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodG.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054929._000008.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodG.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054929._000009.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodG.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054929._000010.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodG.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054929._000011.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodG.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054929._000012.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodG.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054929._000013.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodG.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054929._000014.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodH.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054932._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodH.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054932._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodH.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054932._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodH.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054932._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodH.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054932._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodH.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054932._000006.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054936._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054936._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054936._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054936._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054936._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054936._000006.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054936._000007.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054936._000008.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054936._000009.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054936._000010.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054936._000011.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054936._000012.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054936._000013.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054936._000014.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054936._000015.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054936._000016.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054936._000017.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054936._000018.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data15_13TeV.periodJ.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054936._000019.tree.root");

} else if(!m_MC && m_data17 && !m_isjjj && !m_signal) {
    
    if (m_onGrid==false){ pathFiles="/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijetISR/ntuples/data_dijetgamma/";} // dijet gamma-data17 

    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00327342.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972870._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00327490.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972868._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00327582.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972866._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00327636.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972865._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00327662.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972864._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00327745.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972860._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00327761.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972858._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00327764.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972857._000002.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00327860.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972856._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00327862.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972853._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00328017.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972850._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00328042.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972849._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00328099.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972482._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00328221.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972848._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00328263.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972845._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00328333.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972844._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00328333.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972844._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00328374.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972842._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00328393.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972840._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00329385.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972838._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00329484.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972836._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00329542.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972834._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00329716.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972832._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00329778.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972830._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00329780.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972829._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00329829.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972826._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00329835.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972825._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00329869.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972824._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00329964.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972821._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00330025.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972819._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00330074.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972817._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00330079.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972815._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00330101.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972812._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00330160.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972811._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00330166.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972805._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00330203.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972804._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00330294.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972802._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00330328.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972800._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00330470.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972798._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00330857.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972797._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00330874.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972795._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00330875.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972793._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00331019.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972791._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00331020.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972789._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00331033.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972787._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00331082.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972784._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00331085.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972782._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00331129.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972780._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00331215.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972776._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00331239.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972773._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00331462.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972769._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00331466.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972763._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00331479.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972759._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00331697.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972756._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00331742.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972751._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00331772.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972747._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00331804.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972744._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00331825.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972741._000002.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00331860.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972738._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00331875.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972735._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00331905.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972733._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00331951.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972730._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00331975.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972728._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00332303.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972723._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00332304.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972716._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00332720.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972713._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00332896.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972710._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00332915.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972704._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00332953.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972700._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00332955.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972698._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00333181.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972692._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00333192.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972689._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00333367.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972686._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00333380.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972683._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00333426.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972679._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00333469.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972677._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00333487.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972676._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00333519.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972674._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00333650.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972670._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00333707.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972668._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00333778.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972667._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00333828.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972661._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00333853.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972659._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00333904.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972655._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00333904.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972655._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00333979.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972650._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00333994.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972646._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00334264.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972644._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00334317.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972638._000002.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00334350.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972636._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00334384.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972632._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00334413.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972628._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00334443.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972623._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00334455.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972621._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00334487.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972617._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00334564.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972614._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00334580.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972613._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00334588.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972611._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00334637.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972607._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00334678.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972606._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00334710.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972604._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00334737.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972599._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00334779.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972597._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00334842.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972593._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00334849.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972592._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00334878.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972590._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00334878.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972590._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00334890.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972588._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00334907.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972587._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00334960.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972585._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00334993.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972583._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00335016.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972580._000001.tree.root.2");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00335022.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972578._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00335056.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972573._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00335082.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972572._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00335083.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972571._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00335131.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972570._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00335170.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972568._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00335177.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972567._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00335177.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972567._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00335222.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972566._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00335282.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972565._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00335290.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972563._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00336497.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972562._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00336505.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972561._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00336506.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972560._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00336548.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972559._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00336567.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972557._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00336630.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972555._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00336678.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972554._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00336719.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972552._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00336782.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972551._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00336832.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972549._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00336852.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972548._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00336852.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972548._000002.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00336852.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972548._000004.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00336852.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972548._000005.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00336915.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972547._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00336927.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972546._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00336944.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972545._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00336998.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972543._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00337005.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972542._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00337052.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972541._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00337107.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972539._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00337156.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972538._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00337176.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972536._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00337215.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972481._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00337263.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972535._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00337335.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972534._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00337335.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972534._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00337371.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972533._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00337404.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972531._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00337451.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972529._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00337491.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972528._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00337542.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972527._000001.tree.root"); 
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00337662.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972526._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00337662.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972526._000002.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00337662.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972526._000003.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00337705.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972524._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00337705.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972524._000002.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00337833.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972523._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00337833.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972523._000002.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00337833.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972523._000003.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338183.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972522._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338183.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972522._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338220.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972521._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338220.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972521._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338259.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972518._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338263.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972516._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338263.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972516._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338349.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972515._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338349.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972515._000002.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338349.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972515._000003.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338377.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972514._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338377.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972514._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338377.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972514._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338377.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972514._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338377.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972514._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338480.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972513._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338480.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972513._000002.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338480.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972513._000003.tree.root");   
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338498.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972512._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338608.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972511._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338608.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972511._000002.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338675.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972510._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338712.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972509._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338712.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972509._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338767.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972508._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338767.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972508._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338834.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972507._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338846.physics_Main.dijetgamma.data17_dijetg_five_17_may_tree.root/user.ecorriga.14139150._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338846.physics_Main.dijetgamma.data17_dijetg_five_17_may_tree.root/user.ecorriga.14139150._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338846.physics_Main.dijetgamma.data17_dijetg_five_17_may_tree.root/user.ecorriga.14139150._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338846.physics_Main.dijetgamma.data17_dijetg_five_17_may_tree.root/user.ecorriga.14139150._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338846.physics_Main.dijetgamma.data17_dijetg_five_17_may_tree.root/user.ecorriga.14139150._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338846.physics_Main.dijetgamma.data17_dijetg_five_17_may_tree.root/user.ecorriga.14139150._000006.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338846.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972506._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338846.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972506._000002.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338897.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972505._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338897.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972505._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338933.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972503._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338933.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972503._000002.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338933.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972503._000003.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338967.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972501._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338987.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972500._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00338987.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972500._000002.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00339037.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972499._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00339037.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972499._000002.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00339070.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972498._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00339070.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972498._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00339070.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972498._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00339070.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972498._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00339205.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972497._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00339346.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972496._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00339387.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972495._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00339387.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972495._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00339396.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972494._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00339396.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972494._000002.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00339435.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972493._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00339435.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972493._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00339500.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972492._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00339500.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972492._000002.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00339535.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972491._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00339562.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972490._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00339590.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972488._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00339758.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972487._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00339758.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972487._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00339849.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972484._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00339849.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972484._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00339957.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972483._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00339957.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972483._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00339957.physics_Main.dijetgamma.dijetgamma-data17-01-may_tree.root/user.ecorriga.13972483._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00340030.physics_Main.dijetgamma.data17_dijetg_five_17_may_tree.root/user.ecorriga.14139081._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00340030.physics_Main.dijetgamma.data17_dijetg_five_17_may_tree.root/user.ecorriga.14139081._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00340030.physics_Main.dijetgamma.data17_dijetg_five_17_may_tree.root/user.ecorriga.14139081._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00340030.physics_Main.dijetgamma.data17_dijetg_five_17_may_tree.root/user.ecorriga.14139081._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00340030.physics_Main.dijetgamma.data17_dijetg_five_17_may_tree.root/user.ecorriga.14139081._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00340030.physics_Main.dijetgamma.data17_dijetg_five_17_may_tree.root/user.ecorriga.14139081._000006.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00340030.physics_Main.dijetgamma.data17_dijetg_five_17_may_tree.root/user.ecorriga.14139081._000007.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00340072.physics_Main.dijetgamma.data17_dijetg_five_17_may_tree.root/user.ecorriga.14139083._000001.tree.root"); 
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00340072.physics_Main.dijetgamma.data17_dijetg_five_17_may_tree.root/user.ecorriga.14139083._000002.tree.root"); 
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00340072.physics_Main.dijetgamma.data17_dijetg_five_17_may_tree.root/user.ecorriga.14139083._000003.tree.root"); 
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00340072.physics_Main.dijetgamma.data17_dijetg_five_17_may_tree.root/user.ecorriga.14139083._000004.tree.root"); 
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00340072.physics_Main.dijetgamma.data17_dijetg_five_17_may_tree.root/user.ecorriga.14139083._000006.tree.root"); 
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00340368.physics_Main.dijetgamma.data17_dijetg_five_17_may_tree.root/user.ecorriga.14139084._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00340368.physics_Main.dijetgamma.data17_dijetg_five_17_may_tree.root/user.ecorriga.14139084._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00340453.physics_Main.dijetgamma.data17_dijetg_five_17_may_tree.root/user.ecorriga.14139080._000001.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00340453.physics_Main.dijetgamma.data17_dijetg_five_17_may_tree.root/user.ecorriga.14139080._000002.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00340453.physics_Main.dijetgamma.data17_dijetg_five_17_may_tree.root/user.ecorriga.14139080._000003.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.00340453.physics_Main.dijetgamma.data17_dijetg_five_17_may_tree.root/user.ecorriga.14139080._000004.tree.root");    
      
} else if(!m_MC && !m_data17 && !m_isjjj && !m_signal) { //data16 for dijet+gamma (PCUBA1)
   
   if (m_onGrid==false){ pathFiles="/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijetISR/ntuples/data_dijetgamma/";} // dijet gamma-data16 

    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodF.physics_Main.dijetgamma.data16_dijetg_broke_17_may_tree.root/user.ecorriga.14139919._000016.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodF.physics_Main.dijetgamma.data16_dijetg_broke_17_may_tree.root/user.ecorriga.14139919._000017.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodF.physics_Main.dijetgamma.data16_dijetg_broke_17_may_tree.root/user.ecorriga.14139919._000018.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodF.physics_Main.dijetgamma.data16_dijetg_broke_17_may_tree.root/user.ecorriga.14139919._000019.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodF.physics_Main.dijetgamma.data16_dijetg_broke_17_may_tree.root/user.ecorriga.14139919._000020.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodF.physics_Main.dijetgamma.data16_dijetg_broke_17_may_tree.root/user.ecorriga.14139919._000021.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodF.physics_Main.dijetgamma.data16_dijetg_broke_17_may_tree.root/user.ecorriga.14139919._000022.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodF.physics_Main.dijetgamma.data16_dijetg_broke_17_may_tree.root/user.ecorriga.14139919._000023.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodF.physics_Main.dijetgamma.data16_dijetg_broke_17_may_tree.root/user.ecorriga.14139919._000024.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodF.physics_Main.dijetgamma.data16_dijetg_broke_17_may_tree.root/user.ecorriga.14139919._000025.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054968._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054968._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054968._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054968._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054968._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054968._000006.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054968._000007.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054968._000008.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054968._000009.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054968._000010.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054968._000011.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054968._000012.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054968._000013.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054968._000014.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054968._000015.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054968._000016.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054968._000017.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054968._000018.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054968._000019.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054968._000020.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054968._000021.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054968._000023.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054972._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054972._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054972._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054972._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054972._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054972._000006.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054972._000007.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054972._000008.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054972._000009.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054972._000010.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054972._000011.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054972._000012.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054972._000013.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054972._000014.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054972._000015.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054972._000016.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054972._000017.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054972._000018.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054972._000019.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054972._000020.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054972._000021.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054972._000022.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054972._000023.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054972._000024.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054972._000025.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054972._000026.tree.root");    
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodK.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054976._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodK.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054976._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodK.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054976._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodK.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054976._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodK.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054976._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodK.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054976._000006.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodK.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054976._000007.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodK.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054976._000008.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodK.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054976._000009.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodK.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054976._000010.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodK.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054976._000011.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodK.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054976._000012.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054982._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054982._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054982._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054982._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054982._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054982._000006.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054982._000007.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054982._000008.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054982._000009.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054982._000010.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054982._000011.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054982._000012.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054982._000013.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054982._000014.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054982._000015.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054982._000016.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054982._000017.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054982._000018.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054982._000019.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054982._000020.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054982._000021.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054982._000022.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054982._000023.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054982._000024.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054982._000025.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054982._000026.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054982._000027.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054982._000028.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054982._000029.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054982._000030.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054982._000031.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054982._000032.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.dijetgamma.data1516_dijetgamma_08_may_tree.root/user.ecorriga.14054982._000033.tree.root");
   
  } else if(!m_MC && !m_data17 && m_isjjj && !m_signal) {
    
    if (m_onGrid==false){ pathFiles="/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijetISR/ntuples/data_trijet/";} // trijet-data16
    std::cout << "Sample data 16" << std::endl;
       
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodA.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055155._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodA.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055155._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodA.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055155._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodA.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055155._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodA.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055155._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodA.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055155._000006.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodA.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055155._000007.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodA.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055155._000008.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodA.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055155._000009.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodA.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055155._000010.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodA.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055155._000011.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodA.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055155._000012.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodA.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055155._000013.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodA.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055155._000014.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodA.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055155._000015.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodA.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055155._000016.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodA.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055155._000017.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodA.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055155._000018.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodA.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055155._000019.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodA.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055155._000020.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodA.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055155._000021.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodA.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055155._000022.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodA.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055155._000023.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodA.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055155._000024.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodA.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055155._000025.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodA.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055155._000026.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodB.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055160._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodB.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055160._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodB.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055160._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodB.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055160._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodB.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055160._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodB.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055160._000006.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodB.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055160._000007.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodB.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055160._000008.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodB.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055160._000009.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodB.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055160._000010.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodB.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055160._000011.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodB.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055160._000012.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodB.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055160._000013.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodB.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055160._000014.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodB.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055160._000015.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodB.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055160._000016.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodB.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055160._000018.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodB.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055160._000019.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodB.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055160._000020.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodB.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055160._000021.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodB.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055160._000022.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodB.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055160._000023.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodB.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055160._000024.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodB.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055160._000025.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodB.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055160._000026.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055163._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055163._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055163._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055163._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055163._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055163._000006.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055163._000007.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055163._000008.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055163._000009.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055163._000010.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055163._000011.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055163._000012.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055163._000013.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055163._000014.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055163._000015.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055163._000016.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055163._000017.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055163._000018.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055163._000019.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055163._000020.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055163._000021.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055163._000022.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055163._000023.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055169._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055169._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055169._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055169._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055169._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055169._000006.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055169._000007.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055169._000008.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055169._000009.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055169._000010.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055169._000011.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055169._000012.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055169._000013.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055169._000014.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055169._000015.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055169._000016.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055169._000017.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055169._000018.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055169._000019.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055169._000020.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055169._000021.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055169._000022.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055169._000023.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055169._000024.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055169._000025.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055169._000026.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055169._000027.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055169._000028.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055169._000029.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055169._000030.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055169._000031.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055169._000032.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055169._000033.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055169._000034.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055181._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055181._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055181._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055181._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055181._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055181._000006.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055181._000007.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055181._000008.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055181._000009.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055188._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055188._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055188._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055188._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055188._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055188._000006.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055188._000007.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055188._000008.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055188._000009.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055188._000010.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055188._000011.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055188._000012.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055188._000013.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055188._000014.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055188._000015.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055188._000016.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055188._000017.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055188._000018.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055188._000019.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055188._000020.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055188._000021.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055191._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055191._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055191._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055191._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055191._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055191._000006.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055191._000007.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055191._000008.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055191._000009.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055191._000010.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055191._000011.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055191._000012.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055191._000013.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055191._000014.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055191._000015.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055191._000016.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055191._000017.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055191._000018.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055191._000019.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055191._000020.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055191._000021.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055191._000022.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055191._000023.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055191._000024.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055191._000025.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodG.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055191._000026.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055199._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055199._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055199._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055199._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055199._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055199._000006.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055199._000007.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055199._000008.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055199._000009.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055199._000010.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055199._000011.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055199._000012.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055199._000013.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055199._000014.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055199._000015.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055199._000016.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055199._000017.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055199._000018.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055199._000019.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055199._000020.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055199._000021.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055199._000023.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055199._000024.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055199._000025.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055199._000026.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055199._000027.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055199._000028.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055199._000029.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055199._000030.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055199._000031.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodI.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055199._000032.tree.root.3");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055205._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055205._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055205._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055205._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055205._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055205._000006.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055205._000007.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055205._000008.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055205._000009.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055205._000010.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055205._000011.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055205._000012.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055205._000013.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055205._000014.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055205._000015.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055205._000016.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055213._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055213._000002.tree.root.2");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055213._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055213._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055213._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055213._000006.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data16_13TeV.periodL.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055213._000007.tree.root");

    
  }if(!m_MC && m_data17 && m_isjjj && !m_signal) {
    
    if (m_onGrid==false){ pathFiles="/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijetISR/ntuples/data_trijet/";} // trijet-data17
    
    std::cout << "Sample data 17" << std::endl;
      
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000006.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000007.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000008.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000009.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000010.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000011.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000012.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000013.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000014.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000015.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000016.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000017.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000018.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000019.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000020.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000021.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000022.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000023.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000024.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000025.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000026.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000027.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000028.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000029.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000030.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000031.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000032.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000033.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000034.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000035.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000036.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000037.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000038.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000039.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000040.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000041.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000042.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000043.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000044.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000045.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000046.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000047.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000048.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000049.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000050.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000051.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000052.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000053.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000054.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000055.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000056.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000057.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000058.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000059.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000060.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000061.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000062.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000063.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000064.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000065.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000066.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000067.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000068.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000069.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000070.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000071.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000072.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000073.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000074.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000075.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000076.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000077.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000078.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000079.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000080.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000081.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000082.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000083.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000084.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000085.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000086.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000087.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000088.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000089.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000090.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000091.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000092.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000093.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000094.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000095.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000096.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000097.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000098.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000099.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000100.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000101.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000102.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000103.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000104.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000105.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000106.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000107.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000108.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000109.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000110.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000111.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000112.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000113.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000115.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000116.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000117.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000118.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000119.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000120.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000121.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000122.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000123.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000124.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000125.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000126.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000127.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodB.physics_Main.trijet.data17_trijet_broke_17_may_tree.root/user.ecorriga.14140011._000128.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055228._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055228._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055228._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055228._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055228._000006.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055228._000008.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055228._000010.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055228._000011.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055228._000012.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055228._000013.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055228._000014.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055228._000015.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055228._000016.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055228._000017.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055228._000018.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055228._000019.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055228._000020.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055228._000021.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055228._000022.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055228._000023.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055228._000024.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055228._000025.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodC.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055228._000026.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000006.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000007.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000008.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000009.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000010.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000011.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000012.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000013.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000014.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000015.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000016.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000017.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000018.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000019.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000020.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000021.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000022.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000023.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000024.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000025.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000026.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000027.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000028.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000029.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000030.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000031.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000032.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000033.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000034.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000035.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000036.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000037.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000038.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000039.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000040.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000041.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodD.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055234._000042.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000006.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000007.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000008.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000009.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000010.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000011.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000012.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000013.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000014.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000015.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000016.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000017.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000018.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000019.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000020.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000021.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000022.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000023.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000024.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000025.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000026.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000027.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000028.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000029.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000030.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000031.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000032.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000033.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000034.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000035.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000036.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000037.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000038.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000039.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000040.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000041.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000042.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000043.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000044.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodE.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055242._000045.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055249._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055249._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055249._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055249._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055249._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055249._000006.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055249._000007.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055249._000008.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055249._000009.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055249._000010.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055249._000011.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055249._000012.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055249._000013.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055249._000014.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055249._000015.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055249._000016.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055249._000017.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055249._000018.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055249._000019.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055249._000020.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055249._000021.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055249._000022.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055249._000023.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055249._000024.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055249._000025.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodF.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055249._000026.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodH.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055254._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodH.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055254._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodH.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055254._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodH.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055254._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodH.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055254._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodH.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055254._000006.tree.root");
    //Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodH.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055254._000007.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodH.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055254._000008.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodH.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055254._000009.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodH.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055254._000010.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodH.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055254._000011.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodH.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055254._000012.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodH.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055254._000013.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodH.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055254._000014.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodI.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055262._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodI.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055262._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodI.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055262._000006.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodI.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055262._000007.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodI.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055262._000008.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000006.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000007.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000008.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000009.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000010.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000011.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000012.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000013.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000014.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000015.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000016.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000017.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000018.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000019.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000020.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000021.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000022.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000023.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000024.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000025.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000026.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000027.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000028.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000029.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000030.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000031.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000032.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000033.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000034.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000035.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000036.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000037.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000038.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000039.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000040.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000041.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000042.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000043.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000044.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000045.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000046.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000047.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000048.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000049.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000050.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000053.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000054.tree.root.2");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000055.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000056.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000057.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000058.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000059.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000060.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000061.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000062.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000063.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000064.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000065.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000066.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000067.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000068.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000069.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000070.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000071.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000072.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000073.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000074.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000075.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000076.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000077.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000078.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000079.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000080.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000081.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000082.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000085.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000086.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000087.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000088.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000089.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000090.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000091.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000092.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000093.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000094.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000095.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000096.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000097.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000098.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000099.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000100.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000101.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000102.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000103.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000104.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000105.tree.root.3");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000106.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000107.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000108.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000109.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000111.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000112.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000113.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000114.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.data17_13TeV.periodK.physics_Main.trijet.data151617_trijet_08_may_tree.root/user.ecorriga.14055269._000115.tree.root");
      	
  }else if (m_MC && m_isjjj  && m_signal){ //EOS   //same for data 17/16
//     Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodB.physics_Main.trijet.data.03.08.2017_tree.root/*.root"); //CHANGE
 
    if (m_onGrid==false){ pathFiles="/eos/atlas/user/m/mdevesa/Signal/";}

    std::cout << "Sample Signal jjj" << std::endl;
	
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305534.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp25_mD10_gSp1_gD1.trijet.20171023-01_tree.root/user.kkrizka.12412539._000001.tree.root"); 
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305535.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp25_mD10_gSp2_gD1.trijet.20171023-01_tree.root/user.kkrizka.12412540._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305536.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp25_mD10_gSp3_gD1.trijet.20171023-01_tree.root/user.kkrizka.12412541._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305537.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp25_mD10_gSp4_gD1.trijet.20171023-01_tree.root/user.kkrizka.12412542._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305538.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp35_mD10_gSp1_gD1.trijet.20171023-01_tree.root/user.kkrizka.12412543._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305539.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp35_mD10_gSp2_gD1.trijet.20171023-01_tree.root/user.kkrizka.12412544._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305540.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp35_mD10_gSp3_gD1.trijet.20171023-01_tree.root/user.kkrizka.12412545._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305541.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp35_mD10_gSp4_gD1.trijet.20171023-01_tree.root/user.kkrizka.12412546._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305542.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp45_mD10_gSp1_gD1.trijet.20171023-01_tree.root/user.kkrizka.12412547._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305543.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp45_mD10_gSp2_gD1.trijet.20171023-01_tree.root/user.kkrizka.12412548._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305544.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp45_mD10_gSp3_gD1.trijet.20171023-01_tree.root/user.kkrizka.12412549._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305545.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp45_mD10_gSp4_gD1.trijet.20171023-01_tree.root/user.kkrizka.12412550._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305546.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp55_mD10_gSp1_gD1.trijet.20171023-01_tree.root/user.kkrizka.12412551._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305547.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp55_mD10_gSp2_gD1.trijet.20171023-01_tree.root/user.kkrizka.12412552._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305548.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp55_mD10_gSp3_gD1.trijet.20171023-01_tree.root/user.kkrizka.12412553._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305549.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp55_mD10_gSp4_gD1.trijet.20171023-01_tree.root/user.kkrizka.12412554._000001.tree.root");
      
  }else if (m_MC && !m_signal && m_isjjj){ ///EOS
  
  
    std::cout << "Sample MC jjj" << std::endl;
    
//MC16a

    if (m_onGrid==false){ pathFiles="/eos/user/g/gang/public/Dijet_ISR/mc16a/mc16a_trijet/";}//MC16a

    Files.push_back(pathFiles+"user.ecorriga.mc16a.426131.Sherpa_CT10_jets_JZ1.trijet.mc16a_trijet_14_may_fix_tree.root/user.ecorriga.14105364._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.mc16a.426131.Sherpa_CT10_jets_JZ1.trijet.mc16a_trijet_14_may_fix_tree.root/user.ecorriga.14105364._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.mc16a.426132.Sherpa_CT10_jets_JZ2.trijet.mc16a_trijet_14_may_fix_tree.root/user.ecorriga.14105366._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.mc16a.426132.Sherpa_CT10_jets_JZ2.trijet.mc16a_trijet_14_may_fix_tree.root/user.ecorriga.14105366._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.mc16a.426132.Sherpa_CT10_jets_JZ2.trijet.mc16a_trijet_14_may_fix_tree.root/user.ecorriga.14105366._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.mc16a.426133.Sherpa_CT10_jets_JZ3.trijet.mc16a_trijet_14_may_fix_tree.root/user.ecorriga.14105370._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.mc16a.426133.Sherpa_CT10_jets_JZ3.trijet.mc16a_trijet_14_may_fix_tree.root/user.ecorriga.14105370._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.mc16a.426133.Sherpa_CT10_jets_JZ3.trijet.mc16a_trijet_14_may_fix_tree.root/user.ecorriga.14105370._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.mc16a.426133.Sherpa_CT10_jets_JZ3.trijet.mc16a_trijet_14_may_fix_tree.root/user.ecorriga.14105370._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.mc16a.426133.Sherpa_CT10_jets_JZ3.trijet.mc16a_trijet_14_may_fix_tree.root/user.ecorriga.14105370._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.mc16a.426134.Sherpa_CT10_jets_JZ4.trijet.mc16a_trijet_14_may_fix_tree.root/user.ecorriga.14105372._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.mc16a.426134.Sherpa_CT10_jets_JZ4.trijet.mc16a_trijet_14_may_fix_tree.root/user.ecorriga.14105372._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.mc16a.426134.Sherpa_CT10_jets_JZ4.trijet.mc16a_trijet_14_may_fix_tree.root/user.ecorriga.14105372._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.mc16a.426134.Sherpa_CT10_jets_JZ4.trijet.mc16a_trijet_14_may_fix_tree.root/user.ecorriga.14105372._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.mc16a.426134.Sherpa_CT10_jets_JZ4.trijet.mc16a_trijet_14_may_fix_tree.root/user.ecorriga.14105372._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.mc16a.426135.Sherpa_CT10_jets_JZ5.trijet.mc16a_trijet_14_may_fix_tree.root/user.ecorriga.14105378._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.mc16a.426135.Sherpa_CT10_jets_JZ5.trijet.mc16a_trijet_14_may_fix_tree.root/user.ecorriga.14105378._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.mc16a.426135.Sherpa_CT10_jets_JZ5.trijet.mc16a_trijet_14_may_fix_tree.root/user.ecorriga.14105378._000003.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.mc16a.426135.Sherpa_CT10_jets_JZ5.trijet.mc16a_trijet_14_may_fix_tree.root/user.ecorriga.14105378._000004.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.mc16a.426135.Sherpa_CT10_jets_JZ5.trijet.mc16a_trijet_14_may_fix_tree.root/user.ecorriga.14105378._000005.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.mc16a.426136.Sherpa_CT10_jets_JZ6.trijet.mc16a_trijet_14_may_fix_tree.root/user.ecorriga.14105380._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.mc16a.426136.Sherpa_CT10_jets_JZ6.trijet.mc16a_trijet_14_may_fix_tree.root/user.ecorriga.14105380._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.mc16a.426137.Sherpa_CT10_jets_JZ7.trijet.mc16a_trijet_14_may_fix_tree.root/user.ecorriga.14105383._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.mc16a.426137.Sherpa_CT10_jets_JZ7.trijet.mc16a_trijet_14_may_fix_tree.root/user.ecorriga.14105383._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.mc16a.426138.Sherpa_CT10_jets_JZ8.trijet.mc16a_trijet_14_may_fix_tree.root/user.ecorriga.14105388._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.mc16a.426138.Sherpa_CT10_jets_JZ8.trijet.mc16a_trijet_14_may_fix_tree.root/user.ecorriga.14105388._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.mc16a.426139.Sherpa_CT10_jets_JZ9.trijet.mc16a_trijet_14_may_fix_tree.root/user.ecorriga.14105391._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.mc16a.426139.Sherpa_CT10_jets_JZ9.trijet.mc16a_trijet_14_may_fix_tree.root/user.ecorriga.14105391._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.mc16a.426140.Sherpa_CT10_jets_JZ10.trijet.mc16a_trijet_14_may_fix_tree.root/user.ecorriga.14105395._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.mc16a.426140.Sherpa_CT10_jets_JZ10.trijet.mc16a_trijet_14_may_fix_tree.root/user.ecorriga.14105395._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.mc16a.426141.Sherpa_CT10_jets_JZ11.trijet.mc16a_trijet_14_may_fix_tree.root/user.ecorriga.14105396._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.mc16a.426141.Sherpa_CT10_jets_JZ11.trijet.mc16a_trijet_14_may_fix_tree.root/user.ecorriga.14105396._000002.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.mc16a.426142.Sherpa_CT10_jets_JZ12.trijet.mc16a_trijet_14_may_fix_tree.root/user.ecorriga.14105398._000001.tree.root");
    Files.push_back(pathFiles+"user.ecorriga.mc16a.426142.Sherpa_CT10_jets_JZ12.trijet.mc16a_trijet_14_may_fix_tree.root/user.ecorriga.14105398._000002.tree.root");

/*
//MC16d
    
   if (m_onGrid==false){ pathFiles="/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijetISR/ntuples/mc16d_trijet/";}//MC16d

	Files.push_back(pathFiles+"user.kpachal.mc16d.361039.Sherpa_CT10_SinglePhotonPt35_70_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072008._000001.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361039.Sherpa_CT10_SinglePhotonPt35_70_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072008._000002.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361039.Sherpa_CT10_SinglePhotonPt35_70_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072008._000003.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361039.Sherpa_CT10_SinglePhotonPt35_70_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072008._000004.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361039.Sherpa_CT10_SinglePhotonPt35_70_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072008._000005.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361039.Sherpa_CT10_SinglePhotonPt35_70_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072008._000006.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361039.Sherpa_CT10_SinglePhotonPt35_70_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072008._000007.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361039.Sherpa_CT10_SinglePhotonPt35_70_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072008._000008.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361040.Sherpa_CT10_SinglePhotonPt35_70_CFilterBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072029._000001.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361040.Sherpa_CT10_SinglePhotonPt35_70_CFilterBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072029._000002.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361040.Sherpa_CT10_SinglePhotonPt35_70_CFilterBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072029._000003.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361040.Sherpa_CT10_SinglePhotonPt35_70_CFilterBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072029._000004.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361040.Sherpa_CT10_SinglePhotonPt35_70_CFilterBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072029._000005.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361040.Sherpa_CT10_SinglePhotonPt35_70_CFilterBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072029._000006.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361041.Sherpa_CT10_SinglePhotonPt35_70_BFilter.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072047._000001.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361041.Sherpa_CT10_SinglePhotonPt35_70_BFilter.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072047._000002.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361041.Sherpa_CT10_SinglePhotonPt35_70_BFilter.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072047._000003.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361042.Sherpa_CT10_SinglePhotonPt70_140_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072013._000001.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361042.Sherpa_CT10_SinglePhotonPt70_140_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072013._000002.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361043.Sherpa_CT10_SinglePhotonPt70_140_CFilterBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072053._000001.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361043.Sherpa_CT10_SinglePhotonPt70_140_CFilterBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072053._000002.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361044.Sherpa_CT10_SinglePhotonPt70_140_BFilter.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14071986._000001.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361044.Sherpa_CT10_SinglePhotonPt70_140_BFilter.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14071986._000002.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361045.Sherpa_CT10_SinglePhotonPt140_280_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072038._000001.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361045.Sherpa_CT10_SinglePhotonPt140_280_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072038._000002.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361045.Sherpa_CT10_SinglePhotonPt140_280_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072038._000003.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361045.Sherpa_CT10_SinglePhotonPt140_280_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072038._000004.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361045.Sherpa_CT10_SinglePhotonPt140_280_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072038._000005.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361045.Sherpa_CT10_SinglePhotonPt140_280_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072038._000006.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361046.Sherpa_CT10_SinglePhotonPt140_280_CFilterBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072026._000001.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361046.Sherpa_CT10_SinglePhotonPt140_280_CFilterBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072026._000002.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361046.Sherpa_CT10_SinglePhotonPt140_280_CFilterBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072026._000003.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361047.Sherpa_CT10_SinglePhotonPt140_280_BFilter.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072016._000001.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361047.Sherpa_CT10_SinglePhotonPt140_280_BFilter.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072016._000002.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361047.Sherpa_CT10_SinglePhotonPt140_280_BFilter.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072016._000003.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361048.Sherpa_CT10_SinglePhotonPt280_500_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072020._000001.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361048.Sherpa_CT10_SinglePhotonPt280_500_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072020._000002.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361048.Sherpa_CT10_SinglePhotonPt280_500_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072020._000003.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361049.Sherpa_CT10_SinglePhotonPt280_500_CFilterBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072033._000001.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361049.Sherpa_CT10_SinglePhotonPt280_500_CFilterBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072033._000002.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361050.Sherpa_CT10_SinglePhotonPt280_500_BFilter.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14071979._000002.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361051.Sherpa_CT10_SinglePhotonPt500_1000_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072022._000001.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361052.Sherpa_CT10_SinglePhotonPt500_1000_CFilterBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072051._000001.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361053.Sherpa_CT10_SinglePhotonPt500_1000_BFilter.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14071995._000001.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361054.Sherpa_CT10_SinglePhotonPt1000_2000_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072040._000001.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361055.Sherpa_CT10_SinglePhotonPt1000_2000_CFilterBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14071992._000001.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361056.Sherpa_CT10_SinglePhotonPt1000_2000_BFilter.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072045._000001.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361057.Sherpa_CT10_SinglePhotonPt2000_4000_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14071974._000001.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361058.Sherpa_CT10_SinglePhotonPt2000_4000_CFilterBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072004._000001.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361059.Sherpa_CT10_SinglePhotonPt2000_4000_BFilter.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14071973._000001.tree.root");
*/


 }else if (m_MC && m_signal && !m_isjjj){ //EOS  // gjj
//     Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodB.physics_Main.trijet.data.03.08.2017_tree.root/*.root"); //CHANGE
  
    if (m_onGrid==false){ pathFiles="/1/mdaneri/Signal_dijetgamma/";}

    std::cout << "Sample signal gjj" << std::endl;

    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305161.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp3_mD10_gSp3_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412632._000001.tree.root"); 
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305162.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp4_mD10_gSp3_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412640._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305163.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp5_mD10_gSp3_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412642._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305465.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp25_mD10_gSp3_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412643._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305466.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp25_mD10_gSp2_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412644._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305467.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp25_mD10_gSp1_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412646._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305468.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp35_mD10_gSp3_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412647._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305469.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp35_mD10_gSp2_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412648._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305470.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp35_mD10_gSp1_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412651._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305471.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp45_mD10_gSp3_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412652._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305472.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp45_mD10_gSp2_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412653._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305473.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp45_mD10_gSp1_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412655._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305474.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp55_mD10_gSp3_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412656._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305475.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp55_mD10_gSp2_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412657._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305476.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp55_mD10_gSp1_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412659._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305477.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp75_mD10_gSp4_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412660._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305478.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp75_mD10_gSp3_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412661._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305479.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp75_mD10_gSp2_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412663._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305481.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp95_mD10_gSp3_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412664._000002.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305482.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mR1p5_mD10_gSp4_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412665._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305483.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mR1p5_mD10_gSp3_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412667._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.308967.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph50_mRp25_mD10_gSp2_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412668._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.308968.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph50_mRp35_mD10_gSp2_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412669._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.308969.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph50_mRp45_mD10_gSp2_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412671._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.308970.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph50_mRp55_mD10_gSp2_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412672._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.308971.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph50_mRp75_mD10_gSp2_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412673._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.308972.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph50_mRp95_mD10_gSp2_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412675._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.308973.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph50_mR1p5_mD10_gSp2_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412676._000001.tree.root");
    
  }else if (m_MC && !m_signal && !m_isjjj){ ///EOS   //Gjj
  
  
    std::cout << "Sample mc gjj" << std::endl;
    
//MC16a
/*
    if (m_onGrid==false){ pathFiles="/eos/user/g/gang/public/Dijet_ISR/mc16a/mc16a_dijetgamma/";}//MC16a

	Files.push_back(pathFiles+"user.ecorriga.mc16a.361039.Sherpa_CT10_SinglePhotonPt35_70_CVetoBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105230._000001.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361039.Sherpa_CT10_SinglePhotonPt35_70_CVetoBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105230._000002.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361039.Sherpa_CT10_SinglePhotonPt35_70_CVetoBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105230._000003.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361039.Sherpa_CT10_SinglePhotonPt35_70_CVetoBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105230._000004.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361039.Sherpa_CT10_SinglePhotonPt35_70_CVetoBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105230._000005.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361039.Sherpa_CT10_SinglePhotonPt35_70_CVetoBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105230._000006.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361039.Sherpa_CT10_SinglePhotonPt35_70_CVetoBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105230._000007.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361039.Sherpa_CT10_SinglePhotonPt35_70_CVetoBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105230._000008.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361039.Sherpa_CT10_SinglePhotonPt35_70_CVetoBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105230._000009.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361039.Sherpa_CT10_SinglePhotonPt35_70_CVetoBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105230._000010.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361040.Sherpa_CT10_SinglePhotonPt35_70_CFilterBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105234._000001.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361040.Sherpa_CT10_SinglePhotonPt35_70_CFilterBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105234._000002.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361040.Sherpa_CT10_SinglePhotonPt35_70_CFilterBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105234._000003.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361040.Sherpa_CT10_SinglePhotonPt35_70_CFilterBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105234._000004.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361040.Sherpa_CT10_SinglePhotonPt35_70_CFilterBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105234._000005.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361041.Sherpa_CT10_SinglePhotonPt35_70_BFilter.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105236._000001.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361041.Sherpa_CT10_SinglePhotonPt35_70_BFilter.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105236._000002.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361041.Sherpa_CT10_SinglePhotonPt35_70_BFilter.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105236._000003.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361042.Sherpa_CT10_SinglePhotonPt70_140_CVetoBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105239._000001.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361042.Sherpa_CT10_SinglePhotonPt70_140_CVetoBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105239._000002.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361042.Sherpa_CT10_SinglePhotonPt70_140_CVetoBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105239._000003.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361043.Sherpa_CT10_SinglePhotonPt70_140_CFilterBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105244._000001.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361043.Sherpa_CT10_SinglePhotonPt70_140_CFilterBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105244._000002.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361043.Sherpa_CT10_SinglePhotonPt70_140_CFilterBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105244._000003.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361044.Sherpa_CT10_SinglePhotonPt70_140_BFilter.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105246._000001.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361044.Sherpa_CT10_SinglePhotonPt70_140_BFilter.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105246._000002.tree.root.2");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361045.Sherpa_CT10_SinglePhotonPt140_280_CVetoBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105247._000001.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361045.Sherpa_CT10_SinglePhotonPt140_280_CVetoBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105247._000002.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361045.Sherpa_CT10_SinglePhotonPt140_280_CVetoBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105247._000003.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361045.Sherpa_CT10_SinglePhotonPt140_280_CVetoBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105247._000004.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361046.Sherpa_CT10_SinglePhotonPt140_280_CFilterBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105253._000001.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361046.Sherpa_CT10_SinglePhotonPt140_280_CFilterBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105253._000002.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361046.Sherpa_CT10_SinglePhotonPt140_280_CFilterBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105253._000003.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361046.Sherpa_CT10_SinglePhotonPt140_280_CFilterBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105253._000004.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361047.Sherpa_CT10_SinglePhotonPt140_280_BFilter.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105254._000001.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361047.Sherpa_CT10_SinglePhotonPt140_280_BFilter.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105254._000002.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361047.Sherpa_CT10_SinglePhotonPt140_280_BFilter.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105254._000003.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361048.Sherpa_CT10_SinglePhotonPt280_500_CVetoBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105258._000001.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361048.Sherpa_CT10_SinglePhotonPt280_500_CVetoBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105258._000002.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361048.Sherpa_CT10_SinglePhotonPt280_500_CVetoBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105258._000003.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361049.Sherpa_CT10_SinglePhotonPt280_500_CFilterBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105261._000001.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361049.Sherpa_CT10_SinglePhotonPt280_500_CFilterBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105261._000005.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361049.Sherpa_CT10_SinglePhotonPt280_500_CFilterBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105261._000006.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361050.Sherpa_CT10_SinglePhotonPt280_500_BFilter.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105263._000001.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361050.Sherpa_CT10_SinglePhotonPt280_500_BFilter.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105263._000002.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361051.Sherpa_CT10_SinglePhotonPt500_1000_CVetoBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105265._000001.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361051.Sherpa_CT10_SinglePhotonPt500_1000_CVetoBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105265._000002.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361052.Sherpa_CT10_SinglePhotonPt500_1000_CFilterBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105270._000001.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361052.Sherpa_CT10_SinglePhotonPt500_1000_CFilterBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105270._000002.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361053.Sherpa_CT10_SinglePhotonPt500_1000_BFilter.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105274._000001.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361053.Sherpa_CT10_SinglePhotonPt500_1000_BFilter.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105274._000002.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361054.Sherpa_CT10_SinglePhotonPt1000_2000_CVetoBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105275._000001.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361054.Sherpa_CT10_SinglePhotonPt1000_2000_CVetoBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105275._000002.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361055.Sherpa_CT10_SinglePhotonPt1000_2000_CFilterBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105280._000001.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361055.Sherpa_CT10_SinglePhotonPt1000_2000_CFilterBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105280._000002.tree.root.2");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361056.Sherpa_CT10_SinglePhotonPt1000_2000_BFilter.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105281._000001.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361056.Sherpa_CT10_SinglePhotonPt1000_2000_BFilter.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105281._000002.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361057.Sherpa_CT10_SinglePhotonPt2000_4000_CVetoBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105283._000001.tree.root.2");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361057.Sherpa_CT10_SinglePhotonPt2000_4000_CVetoBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105283._000002.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361058.Sherpa_CT10_SinglePhotonPt2000_4000_CFilterBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105287._000001.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361058.Sherpa_CT10_SinglePhotonPt2000_4000_CFilterBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105287._000002.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361059.Sherpa_CT10_SinglePhotonPt2000_4000_BFilter.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105291._000001.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361060.Sherpa_CT10_SinglePhotonPt4000_CVetoBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105294._000001.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361061.Sherpa_CT10_SinglePhotonPt4000_CFilterBVeto.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105298._000001.tree.root");
	Files.push_back(pathFiles+"user.ecorriga.mc16a.361062.Sherpa_CT10_SinglePhotonPt4000_BFilter.dijetgamma.mc16a_dijetg_14_may_fix_tree.root/user.ecorriga.14105300._000001.tree.root");

*/

//MC16d

   if (m_onGrid==false){ pathFiles="/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijetISR/ntuples/mc16d_dijetgamma/";}//MC16d

	Files.push_back(pathFiles+"user.kpachal.mc16d.361039.Sherpa_CT10_SinglePhotonPt35_70_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072008._000001.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361039.Sherpa_CT10_SinglePhotonPt35_70_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072008._000002.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361039.Sherpa_CT10_SinglePhotonPt35_70_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072008._000003.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361039.Sherpa_CT10_SinglePhotonPt35_70_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072008._000004.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361039.Sherpa_CT10_SinglePhotonPt35_70_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072008._000005.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361039.Sherpa_CT10_SinglePhotonPt35_70_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072008._000006.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361039.Sherpa_CT10_SinglePhotonPt35_70_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072008._000007.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361039.Sherpa_CT10_SinglePhotonPt35_70_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072008._000008.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361040.Sherpa_CT10_SinglePhotonPt35_70_CFilterBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072029._000001.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361040.Sherpa_CT10_SinglePhotonPt35_70_CFilterBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072029._000002.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361040.Sherpa_CT10_SinglePhotonPt35_70_CFilterBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072029._000003.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361040.Sherpa_CT10_SinglePhotonPt35_70_CFilterBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072029._000004.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361040.Sherpa_CT10_SinglePhotonPt35_70_CFilterBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072029._000005.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361040.Sherpa_CT10_SinglePhotonPt35_70_CFilterBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072029._000006.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361041.Sherpa_CT10_SinglePhotonPt35_70_BFilter.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072047._000001.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361041.Sherpa_CT10_SinglePhotonPt35_70_BFilter.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072047._000002.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361041.Sherpa_CT10_SinglePhotonPt35_70_BFilter.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072047._000003.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361042.Sherpa_CT10_SinglePhotonPt70_140_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072013._000001.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361042.Sherpa_CT10_SinglePhotonPt70_140_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072013._000002.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361043.Sherpa_CT10_SinglePhotonPt70_140_CFilterBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072053._000001.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361043.Sherpa_CT10_SinglePhotonPt70_140_CFilterBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072053._000002.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361044.Sherpa_CT10_SinglePhotonPt70_140_BFilter.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14071986._000001.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361044.Sherpa_CT10_SinglePhotonPt70_140_BFilter.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14071986._000002.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361045.Sherpa_CT10_SinglePhotonPt140_280_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072038._000001.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361045.Sherpa_CT10_SinglePhotonPt140_280_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072038._000002.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361045.Sherpa_CT10_SinglePhotonPt140_280_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072038._000003.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361045.Sherpa_CT10_SinglePhotonPt140_280_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072038._000004.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361045.Sherpa_CT10_SinglePhotonPt140_280_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072038._000005.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361045.Sherpa_CT10_SinglePhotonPt140_280_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072038._000006.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361046.Sherpa_CT10_SinglePhotonPt140_280_CFilterBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072026._000001.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361046.Sherpa_CT10_SinglePhotonPt140_280_CFilterBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072026._000002.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361046.Sherpa_CT10_SinglePhotonPt140_280_CFilterBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072026._000003.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361047.Sherpa_CT10_SinglePhotonPt140_280_BFilter.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072016._000001.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361047.Sherpa_CT10_SinglePhotonPt140_280_BFilter.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072016._000002.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361047.Sherpa_CT10_SinglePhotonPt140_280_BFilter.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072016._000003.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361048.Sherpa_CT10_SinglePhotonPt280_500_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072020._000001.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361048.Sherpa_CT10_SinglePhotonPt280_500_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072020._000002.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361048.Sherpa_CT10_SinglePhotonPt280_500_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072020._000003.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361049.Sherpa_CT10_SinglePhotonPt280_500_CFilterBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072033._000001.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361049.Sherpa_CT10_SinglePhotonPt280_500_CFilterBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072033._000002.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361050.Sherpa_CT10_SinglePhotonPt280_500_BFilter.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14071979._000002.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361051.Sherpa_CT10_SinglePhotonPt500_1000_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072022._000001.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361052.Sherpa_CT10_SinglePhotonPt500_1000_CFilterBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072051._000001.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361053.Sherpa_CT10_SinglePhotonPt500_1000_BFilter.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14071995._000001.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361054.Sherpa_CT10_SinglePhotonPt1000_2000_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072040._000001.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361055.Sherpa_CT10_SinglePhotonPt1000_2000_CFilterBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14071992._000001.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361056.Sherpa_CT10_SinglePhotonPt1000_2000_BFilter.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072045._000001.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361057.Sherpa_CT10_SinglePhotonPt2000_4000_CVetoBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14071974._000001.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361058.Sherpa_CT10_SinglePhotonPt2000_4000_CFilterBVeto.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14072004._000001.tree.root");
	Files.push_back(pathFiles+"user.kpachal.mc16d.361059.Sherpa_CT10_SinglePhotonPt2000_4000_BFilter.dijetgamma.2018.05.10.round2_tree.root/user.kpachal.14071973._000001.tree.root");


 }
  
   std::cout << "Files  : " << Files.size() << std::endl;

    return;

}// end GetListOfRootFiles

