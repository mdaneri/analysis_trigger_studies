#!/bin/sh

########################################################################
#
# Purpose: generate the executable "run_exe" whose source code is run.C
# 
# (1) make sure that compile.sh is executable, if not do: 
#	> chmod 755 compile.sh 
# (2) compile run.C to make the executable "run_exe":
#	> ./compile.sh
# (3) run the code, for that try:
#	> ./run_exe --help
#
########################################################################

rm -f Run_Trig *~
g++ Trig.cxx -o Run_Trig `root-config --cflags --libs --glibs` 

